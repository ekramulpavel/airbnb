const Car = require('./Car');

class FancyCar extends Car{

    constructor(brand, model, year){
        super(brand, model, year);
        this.priority = 'High Priority';
    }
    getStatus(){
        console.log('2 hour to finish repair');
    }

    displayCarInformation(){
        console.log('I am super Fancy Car');
        super.displayCarInformation();
    }
}

module.exports = FancyCar;