class Car{
    
    constructor(brand = 'Default Brand', model = 'Default Brand', year){
        this.brand = brand;
        this.model = model;
        this.year = year || 2000;        
    }

    displayCarInformation(){
        console.log(this.brand + ' ' + this.model + ' ' + this.year);
    }
}

module.exports = Car;