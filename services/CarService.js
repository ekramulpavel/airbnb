const Promise = require('../Promise');
const Observable = require('../Observable');

function forEach(array, callbackFunction){
    for(let i = 0; i < array.length; i++){
        const arrayElement = array[i];

        callbackFunction(arrayElement);
    }
}
class CarService{
    constructor(name, country){
        this.name = name;
        this.country = country;
        this.carToRepair = new Array();
    }
    addCar(car){
        car.brand === 'Toyota'? console.log('I cannot add toyota!!!!') : this.carToRepair.push(car);
    }

    displayCustomName(){
        console.log('I am custom name function');
    }

    displayAllCars(){

        forEach(this.carToRepair, (car)=>{

            this.displayCustomName();
            car.displayCarInformation();
        });
        
    }

    getAllCars(){
        return this.carToRepair;
    }

    getSecretDocuments(){
        // return new Promise((resolve, reject)=>{
        //     setTimeout(function(){
        //         let secretDocs = 'SUPER SECRET DOCUMENTS !!!!';
        //         reject(secretDocs);
        //     }, 2000);
        // });

        return new Observable((observer) =>{
            setTimeout(function(){
                let secretDocs = 'SUPER SECRET DOCUMENTS !!!!';
                observer.next(secretDocs);
            }, 2000);
        });
    }
}

module.exports = CarService;